# -*- coding: utf-8 -*-
""" Script de arranque que inicializa la interfaz gráfica
de la App"""
import os
os.environ['KIVY_HOME'] = os.path.dirname(os.path.realpath(__file__))

from logger import logger
from kivy.config import Config
Config.set('graphics', 'width', '1024')
Config.set('graphics', 'height', '768')
from kivy.app import App
from kivy.uix.treeview import TreeViewLabel
from kivy.uix.widget import Widget
from kivy.uix.screenmanager import Screen
import kivy.properties as props
from reportlab import rl_settings
from registro import Registro
from kivy.lang import Builder
from kivy.lang import Factory
from xlrd import open_workbook
from util import mostrar_recomendacion
import sys
import subprocess
import popups
from export import PDFExporter
from kivy.uix.modalview import ModalView
from kivy.uix.boxlayout import BoxLayout
from kivy.core.window import Window

def load_kv():
    Builder.load_file("common.kv")
    Builder.load_file("subquestion.kv")
    Builder.load_file("question.kv")
load_kv()

from question import Question
from question import SimpleQuestion
from PIL import Image, ImageDraw
from kivy.graphics.texture import Texture
from kibot import Kibot
from kivy.uix.label import Label

from kivy.base import Builder
Builder.load_file("common.kv")

class PeiChecklist(App):
    """App principal """
    use_kivy_settings = False
    registro = None # Registro actual
    idx_pregunta_actual = 0
    preg_nodes = props.ListProperty([]) # Listado de widgets de preguntas que hay en el trevview

    with open("VERSION.txt", 'r') as f:
        version = f.read().strip()
        title = props.StringProperty("Herramienta para el diagnóstico y seguimiento del PEI"
                 " (Versión: %s)" % (version))

    def build(self):
        root = super(PeiChecklist, self).build()
        sm = self.root.screen_mgr
        sm.current = "inicio_screen"
        self.manager_preguntas = sm.diligenciamiento_screen.manager_preguntas
        self.navegador = sm.diligenciamiento_screen.navegador
        self.cargar_navegador(self.navegador, self.manager_preguntas)
        Window.bind(on_request_close = self.window_close_cback)
        return root

    def window_close_cback(self, *args):
        """callback que se llama cuando el usuario hace clic en el boton cerrar"""
        popups.Popups.yes_no_popup(title=u'Confirmación', msg=u'¿Está seguro que desea salir?', callback=self.cerrar_ventana)
        return True

    def cerrar_ventana(self, *args):
        btn_text =  args[0].text
        if btn_text == 'Si':
            self.stop()
    
    def mostrar_ventana(self, nombre):
        sm = self.root.screen_mgr
        sm.current = nombre

    def reset(self):
        # Cargar datos de registro actual
        for question in self.questions:
            data = self.registro.get_question_data(question.qid)
            question.load_data(data)
            # Mostrar recomendacion segun respuesta actual
            mostrar_recomendacion(question.sq, question, question.texto_recomendaciones)
        # Resetear treeview
        self.navegador.tview.select_node(self.preg_nodes[0]) # Seleccionar pregunta 1

    def cargar_navegador(self, navegador, preguntas_mgr):
        """Crea las entradas del navegador y carga las preguntas"""
        tview = navegador.tview

        wb = open_workbook('elementos_checklist.xlsx')
        s = wb.sheets()[0]

        # Categorias
        nrows = s.nrows
        #nrows = 2
        categorias_col = [s.cell(x, 0).value for x in range(1, nrows)]
        categorias_desc_col = [s.cell(x, 1).value for x in range(1, nrows)]


        #Preguntas
        preguntas_col = [s.cell(x, 2).value for x in range(1, nrows)]
        descripcion_col = [s.cell(x, 3).value for x in range(1, nrows)]
        recomem_si_col = [s.cell(x, 6).value for x in range(1, nrows)]
        recomem_no_col = [s.cell(x, 7).value for x in range(1, nrows)]
        
        # Treeview
        self.preg_nodes = []
        self.questions = []
        cnt = 0
        for i, cat in enumerate(categorias_col):
            if len(cat):

                ctv = Factory.CategoriaTVLabel(text=cat)
                ctv.idx = cnt
                ctv.bind(is_selected=self.nodo_tview_selected)
                ctv.bind(is_selected=ctv.desplegar)
                last_node = tview.add_node(ctv)
                last_cat = cat

                cat_panel = Factory.ContenedorPregunta()
                cat_panel.titulo_categoria = last_cat if len(last_cat) < 30 else "%s..."%(last_cat[:45])
                cat_panel.cat_text = categorias_desc_col[i]
                sc  = Screen(name='sc_%s'%(cnt))
                sc.add_widget(cat_panel)
                preguntas_mgr.add_widget(sc)

                self.preg_nodes.append(ctv)
                cnt += 1

            if not len(preguntas_col[i]):
                continue
            pregunta = preguntas_col[i]
            ptv = PreguntaTVLabel(text=pregunta)
            ptv.idx = cnt
            ptv.bind(is_selected=self.nodo_tview_selected)
            tview.add_node(ptv, last_node)
            self.preg_nodes.append(ptv)

            cp = Factory.ContenedorPregunta()
            cp.titulo_categoria = last_cat if len(last_cat) < 30 else "%s..."%(last_cat[:45])

            preg_wgt = Factory.PreguntaSiNo(text="[size=30][color=f4fc9f]%s[/color][/size]\n[color=fff]%s[/color]"%(pregunta, descripcion_col[i]))
            preg_wgt.titulo = pregunta
            preg_wgt.categoria = last_cat
            preg_wgt.descripcion = descripcion_col[i]
            preg_wgt.qid = preg_wgt.number = i +1 
            preg_wgt.sq.group = "sino_group_%s"%(preg_wgt.qid)
            preg_wgt.recomem_si = recomem_si_col[i]
            preg_wgt.recomem_no = recomem_no_col[i]
            self.questions.append(preg_wgt)
            cp.container.add_widget(preg_wgt)

            sc  = Screen(name='sc_%s'%(cnt))
            sc.add_widget(cp)
            preguntas_mgr.add_widget(sc)
            cnt += 1

        tview.bind(minimum_height = tview.setter('height'))
        self.total_questions = len(preguntas_col)
        tview.select_node(self.preg_nodes[0]) # Seleccionar pregunta 1

    def nodo_tview_selected(self, preg_tvlabel, value):
        if(value):
            self.goto_question(preg_tvlabel.idx)
            # Abrir nodo padre si no esa desplegado para que se vea este nodo
            if not preg_tvlabel.parent_node.is_open:
                self.navegador.tview.toggle_node(preg_tvlabel.parent_node)

    def goto_question(self, idx=0):
        self.manager_preguntas.current = "sc_%s"%(idx) 
        self.idx_pregunta_actual = idx

    def prev_question(self):
        new_idx = max(0, self.idx_pregunta_actual -1)
        self.navegador.tview.select_node(self.preg_nodes[new_idx])
    
    def next_question(self):
        new_idx = min(self.total_questions-1, self.idx_pregunta_actual +1)
        self.navegador.tview.select_node(self.preg_nodes[new_idx])
    
    def nuevo_registro(self):
        self.registro=Registro()
        self.reset()
        self.mostrar_ventana("diligenciamiento_screen")
        self.title = self.registro.fname

    def abrir_registro(self):
        registro = Registro()
        fname = registro.load()
        if fname == '':
            return
        self.registro = registro
        self.reset()
        self.mostrar_ventana("diligenciamiento_screen")
        self.title = self.registro.fname


    def save(self):
        if self.registro is not None:
            for q in self.questions:
                self.registro.set_question_data(q)
            self.registro.save()
            self.title = self.registro.fname
    
    def generar_reporte(self):
        #Validar
        if self.registro is None:
            return

        self.registro.save()
        #Validar primero
        msgs_error = []
        categories2validate = []

        # Solo categorias que tienen al menos una pregunta respondida
        for question in self.questions:
            try:
                question.validate()
            except:
                pass
            else:
                categories2validate.append(question.categoria)

        categories2validate = set(categories2validate)
        if not len(categories2validate):
            msg= "[size=20]Error no se ha podido generar el reporte por las siguientes razones:[/size] \n[size=20]Por favor diligencie al menos una categoría de preguntas.[/size]"
            popups.Popups.info_popup(title='', msg=msg, size=(0.85, 0.7))
            return


        missing_cats = []
        for question in self.questions:
            try:
                question.validate()
            except Exception as e:
                if question.categoria in categories2validate:
                    missing_cats.append(question.categoria)
                    msgs_error.append("%s / %s: - %s"%(question.categoria, question.titulo, e))
        missing_cats = set(missing_cats)
        r = len(msgs_error)
        #r = False
        if r:
            # notificar errores
            msg= u"[size=20]Error no se ha podido generar el reporte porque faltan preguntas por diligenciar en:[/size] \nCategorías:\n%s\nPreguntas: \n%s\n [size=20]Por favor corrija los errores e intente nuevamente.[/size]"%('\n'.join(missing_cats) ,'\n'.join(msgs_error[:10]))
            popups.Popups.info_popup(title='', msg=msg, size=(0.95, 0.8))

        else:
            #Generar reporte si no hubo errores
            p = PDFExporter()
            ruta = p.export(self.registro, self.questions, "%s.pdf"%(self.registro.fname[:-4]), categories2validate)
            msg= "[size=20]El reporte ha sido generado exitosamente en: [/size]  \n %s"%(ruta)
            popups.Popups.info_popup(title='', msg=msg, size=(0.85, 0.7),  icono='img/icono-pdf-directorio.png')

    def ayuda(self):
        mv = ModalView(size_hint=(1,1), auto_dismiss=True)
        mv.background_color = (0,0,0,0)
        mv.background="img/transparent.png"
        aw = AyudaWidget()
        aw.mv = mv
        aw.app = self
        mv.add_widget(aw)
        mv.open()
        
class CategoriaTVLabel(TreeViewLabel):
    idx = props.NumericProperty(0)

    def desplegar(self, *args):
        if self.parent is not None and self.is_selected:
            self.parent.toggle_node(self)


class PreguntaTVLabel(TreeViewLabel):
    idx = props.NumericProperty(0)

class PreguntaSiNo(SimpleQuestion):
    cheight = props.NumericProperty(0)
    titulo = props.StringProperty('')
    categoria = props.StringProperty('')
    descripcion = props.StringProperty('')

    def on_children(self, obj, value):
        value[0].bind(height=self.increase_size)

    def increase_size(self, obj, value):
        self.cheight = 0
        for c in self.children:
            self.cheight += c.height
        self.height = self.cheight + 120
        if self.parent is not None:
            self.parent.height = self.height + 80

class RecomendacionesLabel(Label):
    def on_ref_press(self, ref):
        url = ref
        print "Open url", url
        try:
            #Linux
            subprocess.call('xdg-open pdf/' + url,shell=True)
        except:
            pass
        try:
            #Windows
            retcode = subprocess.call("start pdf/" + url, shell=True)
            if retcode < 0:
                print >>sys.stderr, "Child was terminated by signal", -retcode
            else:
                print >>sys.stderr, "Child returned", retcode
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e

class AyudaWidget(BoxLayout):
    texture=props.ObjectProperty(None)
    msg = props.StringProperty('')
    mv = props.ObjectProperty(None)
    kb = None
    app = None

    def on_size(self, obj, value):
        if 100 not in value:
            width, height = self.size
            self.img = Image.new('RGBA',(width, height))
            self.draw = ImageDraw.Draw(self.img)
            self.current_scrn = self.app.root.screen_mgr.current
            self.step = 1
            self.coach(self.step)

    def coach(self, step=1):
        width, height = self.size
        self.draw.rectangle((0,0, width, height), fill=(0, 0, 0, int(0.8*255))) #Background

        if step == 1:
            self.highlight_widget('titulo')
            self.msg = "Ayuda de la aplicación. Haga clic en cualquier parte para continuar..."
        elif step == 2:
            self.highlight_widget('nuevo')
            self.msg = "El botón nuevo sirve para crear un nuevo registro. En el título de la ventana aparecerá el nombre por defecto del archivo del registro. Para cambiar el nombre del archivo de registro puede hacer clic en guardar"
        elif step == 3:
            self.highlight_widget('abrir')
            self.msg = "El botón abrir sirve para abrir un registro existente. En el título de la ventana aparecerá el nombre del registro actual."
        elif step == 4:
            self.highlight_widget('guardar')
            self.msg = "El botón guardar almacena el registro actual en el disco duro. Si es un registro nuevo deberá especificar un nombre para el archivo."
            self.app.root.screen_mgr.current = "diligenciamiento_screen"
        elif step == 5:
            self.highlight_widget('diligenciamiento')
            self.msg = "Una vez creado o cargado un registro se muestra la ventana de diligenciamiento. A la izquierda se muestran unos controles para recorrer el listado de preguntas en el formulario y a la derecha la pregunta actual."
        elif step == 6:
            self.highlight_widget('navegador')
            self.msg = 'El navegador permite recorrer el formulario de preguntas. Las preguntas se encuentran agrupadas en categorías. Puede hacer clic en cada categoría para desplegar las preguntas que esta contiene y hacer clic en una de estas para ir a la pregunta respectiva. '
        elif step == 7:
            self.highlight_widget('manager_preguntas')
            self.msg = 'La pregunta tiene una categoría a la cual pertenece, un título y una descripción. Deberá responder Si o No utilizando los botones de respuesta. Según su respuesta se mostrarán recomendaciones correspondientes al item evaluado. Recuerde utilizar la barra de scroll vertical (o la rueda del mouse) para visualizar toda la pregunta y sus recomendaciones'
        elif step == 8:
            self.highlight_widget('manager_preguntas')
            self.msg = 'Diligencie las preguntas de las categorías de su interés. Recuerde que si diligencia al menos una pregunta de una categoría deberá diligenciar el resto de las preguntas en esa categoría para poder generar el reporte.'
        elif step == 9:
            self.highlight_widget('guardar')
            self.msg =  "Recuerde guardar a medida que va diligenciando el registro."
        elif step == 10:
            self.highlight_widget('generar')
            self.msg = "El botón generar permite generar un reporte en formato pdf del registro. El registro deberá estar completamente diligenciado antes de poder generar este reporte. \n El archivo de reporte estará en la misma ubicación que el de registro."
        elif step == 11:
            self.msg = 'Final de la ayuda. Haga clic para terminar.'
        else:
            self.app.root.screen_mgr.current = self.current_scrn
            self.mv.dismiss()

        buf = self.img.tobytes()
        texture = Texture.create(size=(width, height))
        texture.blit_buffer(buf, colorfmt='rgba', bufferfmt='ubyte')
        self.texture = texture
        
        self.step += 1

    def on_touch_down(self, *args):
        self.coach(self.step)

    def highlight_widget(self, hid):
        wid = self.kb.find(hid=hid)
        x, y = wid.pos
        w, h = wid.size
        d = 4
        self.draw.rectangle((x-d, y-d,  x+w+d, y+h+d), fill=(255, 255, 255, 0))

if __name__ in ('__main__', '__android__'):
    logger.info("Aplicación Inicializada.")
    app = PeiChecklist()
    AyudaWidget.kb = Kibot(app)
    app.run()
