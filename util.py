import re

def mostrar_recomendacion(subq, root, label):
   """Actualiza el label de recomendaciones de la respectiva pregunta segun el valor seleccionado"""
   data = subq.get_data()
   if not len(data):
       label.text = ''
       label.bgcolor = (0, 0, 0, 0)
   elif data[0] == 1:
       label.text = format_links(root.recomem_si)
       label.bgcolor = (157/255., 193/255., 23/255., 1)
   else:
       label.text = format_links(root.recomem_no)
       label.bgcolor = (196/255., 22/255., 27./255., 1)


def format_links(text):
    #urls = re.findall('file[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', text)
    urls = re.findall("[a-zA-z]{1,}\.pdf", text)

    for url in urls:
        text = text.replace(url, "[color=#0001ff][ref=%s]%s[/ref][/color]"%(url, url)) #poner cada link dentro de un ref
    #TODO posible error si se repiten links en el texto
    return text
