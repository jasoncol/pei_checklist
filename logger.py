# -*- coding: utf-8 -*-
"""Módulo para Logging interno de la aplicación.
http://victorlin.me/posts/2012/08/26/good-logging-practice-in-python
Uso: from logger import logger
logger.info(msg)
logger.error(msg, exc_info=True)
"""
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# create a file handler
handler = logging.FileHandler('peiforms.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s  - %(levelname)s - [%(module)s - %(funcName)s]:  %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(handler)
