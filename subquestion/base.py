# -*- coding: utf-8 -*-
""" Este m�dulo contiene las deficiones de las subpreguntas. """
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.textinput import TextInput
from kivy.uix.dropdown import DropDown
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.spinner import Spinner
from kivy.uix.button import Button
from kivy.uix.checkbox import CheckBox
from kivy.uix.togglebutton import ToggleButton
from kivy.properties import (ObjectProperty, ListProperty, StringProperty,
                             BooleanProperty, NumericProperty)
from kivy.uix.behaviors import ButtonBehavior
from kivy.factory import Factory
from kivy.uix.popup import Popup
import constants as ct


class SubQuestion(Widget):

    """ Clase padre de la que heredan todas las clases de subpreguntas"""
    sqid = StringProperty("")  # Internal ID of subquestion
    answered = BooleanProperty(False)  # TODO deprecated?
    tag = StringProperty("")
    """Tag para referenciar la subpregunta en mensajes de error de validaci�n"""
    question = ObjectProperty(None)
    optional = BooleanProperty(False)
    """True si el campo es opcional"""
    default_validators = []
    validators = ListProperty([])
    """Listado de validadores"""
    no_info_string = "-"
    """Caracter que usa el usuario cuando no tiene info para ingresar a un campo de texto. Desactiva validadores"""

    def __init__(self, *args, **kwargs):
        super(SubQuestion, self).__init__(*args, **kwargs)
        if 'sqid' in kwargs:
            self.sqid = kwargs['sqid']
        if 'tag' in kwargs:
            self.tag = kwargs['tag']
        if 'validators' in kwargs:
            self.validators =  kwargs['validators']

    def validate(self):
        """Valida el valor de la subpregunta. Debe implementarse para cada
        subclase"""
        props = {'data': self.get_data(), 'tag': self.tag, 'validators': self.validators}
        if hasattr(self, 'group'):
            props['group'] = self.group
        return self.cls_validate(props)

    @classmethod
    def cls_validate(cls, sq_props):
        # Gather validators
        validators = cls.default_validators
        # Get other validators
        if 'validators' in sq_props:
            validators.extend(sq_props['validators'])
        tag = sq_props['tag'] if 'tag' in sq_props else ''

        try:
            if sq_props['data'] == cls.no_info_string:
                return

            for v in validators:
                v.validate(tag, sq_props['data'])
        except EmptyFieldException as e:
            if not('optional' in sq_props and not sq_props['optional']):
                raise e

    def get_data(self):
        """Obtiene el valor de la subpregunta. Debe implementarse para cada
        subclase"""
        raise NotImplementedError

    def load_data(self, data):
        """Carga el valor de la subpregunta. Debe implementarse para cada
        subclase.
        :param data: diccionario, lista o valor.
        """
        raise NotImplementedError

    def mark_error(self, *args, **kwargs):
        """Marca visualmente el widget que ve el usuario si hay errores de validacion"""
        try:
            self.validate()
        except NotImplementedError:
            raise NotImplementedError
        except Exception as e:
            self._paint_error(e)
        else:
            self._paint_error(None)

    def _paint_error(self, error):
        """Pinta el widget que ve el usuario con un color que refleja el estado de error.
        Cada subpregunta debe implementar esto"""
        raise NotImplementedError

    @classmethod
    def get_data_str(cls, **kwargs):
        """Devuelve la respuesta a esta subpregunta en lenguaje humano"""
        raise NotImplementedError

""" Exceptions """


class SubQuestionException(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class EmptyFieldException(SubQuestionException):

    def __init__(self, tag):
        tag = "(%s)" % (tag)
        tag = tag if tag != "()" else ''
        value = "Campos sin diligenciar %s" % (tag)
        super(EmptyFieldException, self).__init__(value)


class NotNumberException(SubQuestionException):

    def __init__(self, tag):
        tag = "(%s)" % (tag)
        tag = tag if tag != "()" else ''
        value = "Numero no valido %s" % (tag)
        super(NotNumberException, self).__init__(value)

""" End Exceptions """


""" Validadores """

class SubQuestionValidator(object):

    def __init__(self, *args, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

    def validate(self, sq_tag, value):
        raise NotImplementedError


class EmptyValueValidator(SubQuestionValidator):

    def validate(self, sq_tag, value):

        if value is None:
            raise EmptyFieldException(sq_tag)

        if (isinstance(value, list) or isinstance(value, tuple)) and (None in value or len(value) == 0):
            raise EmptyFieldException(sq_tag)
            

        value = str(value) 
        if (len(value) < 1):
            raise EmptyFieldException(sq_tag)


class NumberValueValidator(SubQuestionValidator):

    def validate(self, sq_tag, value):
        try:
            num = float(value)
            if num < 0:
                raise Exception
            return True
        except:
            raise NotNumberException(sq_tag)

class IntegerNumberValueValidator(SubQuestionValidator):

    def validate(self, sq_tag, value):
        try:
            if '.' in value  or ',' in value:
                raise Exception
            num = float(value)
            if num < 0:
                raise Exception
            return True
        except:
            raise NotNumberException(sq_tag)
