# -*- coding: utf-8 -*-
from optionsfield import ToggleField
from kivy.factory import Factory
import constants as ct

class DepartamentoToggleField(ToggleField):

    def __init__(self, *args, **kwargs):
        super(DepartamentoToggleField, self).__init__(*args, **kwargs)
        self.use_popup = True
        self.orientation = 'vertical'

        # Cargar Departamentos
        values = []
        values_ids = []
        for x in ct.DEPARTAMENTOS:
            values.append(x[1])
            values_ids.append(x[0])

        self.values.extend(values)
        self.values_ids = values_ids

    def close_popup(self, o):
        """ Obliga la actualizacion de instrument.data uan vez selecciona algo
        para que las otras subpreguntas sepan que departamento fue elegido"""

        super(DepartamentoToggleField, self).close_popup(o)
        # Forzar guardado de instrumento
        if self.question is not None and self.question.instrument is not None:
            self.question.instrument.set_question_data(self.question)
            # Limpiar
            self.question.get_subq(tag='subregion').load_data([])
            self.question.get_subq(tag='municipio').load_data([])

    @classmethod
    def get_data_str(cls, **kwargs):
        values = []
        values_ids = []
        for x in ct.DEPARTAMENTOS:
            values.append(x[1])
            values_ids.append(x[0])
        kwargs['values'] = values
        kwargs['values_ids'] = values_ids
        return super(DepartamentoToggleField, cls).get_data_str(**kwargs)


class SubregionToggleField(ToggleField):

    def __init__(self, *args, **kwargs):
        super(SubregionToggleField, self).__init__(*args, **kwargs)
        self.use_popup = True
        self.orientation = 'vertical'
        self.group = 'subregion_group'

    def close_popup(self, o):
        super(SubregionToggleField, self).close_popup(o)
        # Forzar guardado de instrumento
        if self.question is not None and self.question.instrument is not None:
            self.question.instrument.set_question_data(self.question)
            self.question.get_subq(tag='municipio').load_data([])

    def _load_subregiones_list(self):
        # Lista de subregiones
        data = self.question.instrument.get_question_data(self.question.qid)
        # Obtener id de departamento elegido
        try:
            subq = self.question.get_subq(tag='departamento')
            dep_id = self.question.instrument.get_subq_data(data, subq.sqid)[0]
            if dep_id is None or dep_id == 0:
                raise Exception
        except Exception as e:
            return
        prev_data = self.get_data()
        # Cargar subregiones
        values = []
        values_ids = []
        for x in ct.SUBREGIONES:
            if x[2] == dep_id:  # si subregion pertenece a departamento
                values.append(x[1])
                values_ids.append(x[0])

        self.container_height = 40 * len(values) + 10
        del self.values[:]
        self.values.extend(values)
        self.values_ids = values_ids

        # Marcar subregion seleccionada previamente pues se resetearon los togglebuttons
        if prev_data is not None and len(prev_data):
            try:
                pos = self.values_ids.index(prev_data[0]) 
            except:
                pass # El valor previo no está en los posibles valores actuales
            else:
                self.container.children[-(pos+1)].state = 'down'

    def show_popup(self, o):
        """ Antes de mostrar el listado actualiza los posibles valores segun el
        departamento elegido """
        self._load_subregiones_list()
        super(SubregionToggleField, self).show_popup(o)

    def load_data(self, data):
        # Cargar primero el listado de posibles valores
        self._load_subregiones_list()
        super(SubregionToggleField, self).load_data(data)

    @classmethod
    def get_data_str(cls, **kwargs):
        values = []
        values_ids = []
        for x in ct.SUBREGIONES:
            values.append(x[1])
            values_ids.append(x[0])
        kwargs['values'] = values
        kwargs['values_ids'] = values_ids

        return super(SubregionToggleField, cls).get_data_str(**kwargs)

class MunicipioToggleField(ToggleField):

    def __init__(self, *args, **kwargs):
        super(MunicipioToggleField, self).__init__(*args, **kwargs)
        self.option_class = Factory.ToggleButtonFixedS

    def _load_municipios_list(self):
        # Lista de municipios
        data = self.question.instrument.get_question_data(self.question.qid)
        # Obtener id de subregion o departamento elegido
        # indice para saber si se cheque id de subregion o departamento
        id2check = 2
        try:
            subq = self.question.get_subq(tag='subregion')
            region_id = self.question.instrument.get_subq_data(data, subq.sqid)
            if region_id is None or len(region_id) == 0:
                # si no hay subregion elegida mirar departamento
                subq = self.question.get_subq(tag='departamento')
                dep_id = self.question.instrument.get_subq_data(
                    data, subq.sqid)[0]
                region_id = dep_id
                # se chequeara el id en la posicion 3 de cada entrada de
                # municipio
                id2check = 3
            else:
                region_id = region_id[0]

        except Exception as e:

            return

        prev_data = self.get_data()
        # Cargar municipios
        values = []
        values_ids = []
        for x in ct.MUNICIPIOS:
            if x[id2check] == region_id:  # si municipio pertenece a subregion
                values.append(x[1])
                values_ids.append(x[0])

        self.container_height = 40 * len(values) + 10
        del self.values[:]
        self.values.extend(values)
        self.values_ids = values_ids

        # Marcar municipio seleccionado previamente pues se resetearon los togglebuttons
        if prev_data is not None and len(prev_data):
            try:
                pos = self.values_ids.index(prev_data[0]) 
            except:
                pass # El valor previo no está en los posibles valores actuales
            else:
                self.container.children[-(pos+1)].state = 'down'


    def show_popup(self, o):
        """ Antes de mostrar el listado actualiza los posibles valores segun la
        subregion elegida"""
        self._load_municipios_list()
        super(MunicipioToggleField, self).show_popup(o)

    def close_popup(self, o):
        super(MunicipioToggleField, self).close_popup(o)
        # Forzar guardado de instrumento
        if self.question is not None and self.question.instrument is not None:
            self.question.instrument.set_question_data(self.question)
            # TODO no cargar subr si ya hay subregion o si es la misma
            #obtener subregion de municipio actual
            mun_id = self.get_data()
            if mun_id is not None and len(mun_id):
                mun_id = mun_id[0]
                for x in ct.MUNICIPIOS:
                    if x[0] == mun_id:
                        subr_id = x[2]
                        break
                
                self.question.get_subq(tag='subregion').load_data([subr_id])

    def load_data(self, data):
        # Cargar primero el listado de posibles valores
        self._load_municipios_list()
        print "DATA is", data
        super(MunicipioToggleField, self).load_data(data)

    @classmethod
    def get_data_str(cls, **kwargs):
        values = []
        values_ids = []
        for x in ct.MUNICIPIOS:
            values.append(x[1])
            values_ids.append(x[0])
        kwargs['values'] = values
        kwargs['values_ids'] = values_ids

        return super(MunicipioToggleField, cls).get_data_str(**kwargs)
