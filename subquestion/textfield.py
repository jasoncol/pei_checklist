# -*- coding: utf-8 -*-
from base import SubQuestion, TextInput, EmptyFieldException, NotNumberException, EmptyValueValidator, NumberValueValidator, IntegerNumberValueValidator
from kivy.properties import BooleanProperty
import weakref

class BaseTextInput(TextInput):
    pass
    

class BaseTextInputField(BaseTextInput, SubQuestion):
    default_validators = [EmptyValueValidator()]
    _global_textfield_list = []
    """Lista de todas las instancias existentes de textinput"""

    def __init__(self, *args, **kwargs):
        BaseTextInputField._global_textfield_list.append(weakref.ref(self))
        super(BaseTextInputField, self).__init__(*args, **kwargs)

    def on_disabled(self, obj, value):
        super(BaseTextInputField, self).on_disabled(obj, value)
        # quitar de  la lista gral de focusables solo si se deshabilita
        if self.disabled:
            my_pos = [i for i, x in enumerate(BaseTextInputField._global_textfield_list) if x() is self][0]
            del BaseTextInputField._global_textfield_list[my_pos]

    def on_focus(self, obj, value):
        try:
            super(BaseTextInputField, self).on_focus(obj, value)
        except:
            pass
        if not value:
            self.mark_error()

    #def on_touch_down(self, touch):
    #    BaseTextInput.on_touch_down(self, touch)
    #    FocusBehavior.on_touch_down(self,touch)

    def load_data(self, data):
        if data is None:
            self.text = ''
        else:
            if isinstance(data, int):
                data = str(data)
            elif isinstance(data, float):
                # Preservar decimales solo si hay una cantidad apreciable
                if data - int(data) < 0.1:
                    data = str(int(data))
            self.text = data
            self.mark_error()

    def _paint_error(self, error):
        if error is None:
            self.background_color = (190 / 255., 216 / 255., 1 / 255., 1)
            self.foreground_color = (0 / 255., 0 / 255., 0 / 255., 1)
        elif isinstance(error, EmptyFieldException):
            self.background_color = (180 / 255., 180 / 255., 180 / 255., 1)
        else:
            self.background_color = (250 / 255., 0 / 255., 0 / 255., 1)

    @classmethod
    def get_data_str(cls, **kwargs):
        data =  kwargs['data']
        if data is None:
            return ''

        return data

    def _keyboard_on_key_down(self, window, keycode, text, modifiers):
        '''
        Tomado de FocusBehavior kivy 1.9 y modificado. Si se presiona tab o shift  + tab
        navegar entre los textfields que hay en _global_textfield_list
        '''
        print "se presiono la tecla: ", keycode
        if keycode[1] == 'tab' or (keycode[1] == 'enter' and not self.multiline):  # deal with cycle
            my_pos = [i for i, x in enumerate(BaseTextInputField._global_textfield_list) if x() is self][0]
            if ['shift'] == modifiers and keycode[1] != 'enter':
                next_ = my_pos - 1 if my_pos -1 > -1 else None
            else:
                total_widgets = len(BaseTextInputField._global_textfield_list)
                next_ = my_pos + 1 if my_pos +1 < total_widgets else None
                #next = self._get_focus_next('focus_next')
            if next_ is not None:
                self.focus = False
                BaseTextInputField._global_textfield_list[next_]().focus = True
            return True

        else:
            return super(BaseTextInputField, self)._keyboard_on_key_down(window, keycode, text, modifiers)

class TextField(BaseTextInputField):

    ''' Simple char textfield '''

    def get_data(self):
        return self.text.strip()


class TextArea(BaseTextInputField):

    ''' Multiline textfield '''

    def get_data(self):
        return self.text.strip()


class NumberField(BaseTextInputField):
    default_validators = [EmptyValueValidator(), NumberValueValidator()]

    ''' Numeric Textfield '''

    def get_data(self):
        return self.text.strip()


class IntegerNumberField(NumberField):
    default_validators = [EmptyValueValidator(), IntegerNumberValueValidator()]

class SelectedTextField(TextField):
    def on_touch_down(self,touch):
        super(SelectedTextField, self).on_touch_down(touch)
        if self.collide_point(*touch.pos):
            self.select_all()
    def on_text(self, obj, value):
        self.question.save_btn.text = "Guardar \n (Archivo: ./data/%s.pei)"%(value)
