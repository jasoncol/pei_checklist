# -*- coding: utf-8 -*-
from base import SubQuestion, EmptyValueValidator
from kivy.properties import (ObjectProperty, ListProperty, StringProperty,
                             BooleanProperty, NumericProperty)
from kivy.uix.boxlayout import BoxLayout


class FileField(BoxLayout, SubQuestion):
    default_validators = [EmptyValueValidator()]
    text = StringProperty(" - ")

    def show_filechoose(self):
        # TODO solo para windows
        try:
            from Tkinter import Tk
            from tkFileDialog import askopenfilename
            Tk().withdraw()
            # show an "Open" dialog box and return the path to the selected
            # file
            filename = askopenfilename()
            print(filename)
            if filename is not None:
                self.text = filename
        except:
            pass

    def get_data(self):
        return self.text

    def load_data(self, data):
        if data is None:
            self.text = ''
        else:
            if isinstance(data, int):
                data = str(data)
            elif isinstance(data, float):
                # Preservar decimales solo si hay una cantidad apreciable
                if data - int(data) < 0.1:
                    data = str(int(data))
            self.text = data

    def _paint_error(self, error):
        pass

    @classmethod
    def get_data_str(cls, **kwargs):
        data = kwargs['data']

        if data is None:
            return ''

        return data
