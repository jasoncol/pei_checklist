# -*- coding: utf-8 -*-
from Tkinter import Tk
from tkFileDialog import askopenfilename, asksaveasfilename
import json

class Registro(object):
    data = {}
    """Diccionario con la información que se diligencia
     (respuestas a las preguntas). Usa como llaves (keys) los id de las
     preguntas (y sus subpreguntas)"""

    questions = []
    fname = 'Nuevo registro.chp'
    is_new = True #True si es nuevo y nunca se ha guardado en disco
    options = {}
    options['defaultextension'] = '.chp'
    options['filetypes'] = [('Archivo PEI', '.chp')]

    def __init__(self, *args, **kwargs):
        super(Registro, self).__init__(*args, **kwargs)
        self.data = {}

    def validate():
        raise NotImplementedError

    def set_question_data(self, question):
        """Actualiza la información de la pregunta del instrumento en self.data
        :param question: Instancia Question que tiene el input del usuario.
        """
        qdata = question.get_data()
        self.data["Q%s" % (question.qid)] = qdata
        # print "Data instrument", self.data
        self.validated = False

    def get_question_data(self, qid):
        """ Obtiene la información de la pregunta almacenada en
        self.data.

        :param qid: Id de la pregunta.
        :returns: data de la pregunta (diccionario).
        """

        if "Q%s" % (qid) in self.data:
            return self.data["Q%s" % (qid)]
        return None  # TODO exception ? noquestiondatafound

    def get_subq_data(self, data, sqid):
        """ Devuelve data de Subquestion con id sqid en diccionario de entrada
        data
        :param data: información (diccionario).
        :param sqid: id de subpregunta.
        :returns: data de subpregunta (puede ser texto, numero o list) o None.
        """
        key = "SQID-%s" % (sqid) if 'SQID' not in str(sqid) else str(sqid)
        if data is None:
            return None
        if key in data:
            return data[key]
        return None

    def saveas_popup(self):
        # get filename
        try:
            Tk().withdraw()
            # show an "Open" dialog box and return the path to the selected
            # file
            #filename = askopenfilename()
            self.options['title'] = 'Escoja nombre de archivo para guardar el registro'
            self.options['initialfile'] = self.fname
            filename = asksaveasfilename(**self.options)
            if filename == '' or not len(filename):
                raise Exception
        except:
            return False
        return filename

    def load_popup(self):
        try:
            Tk().withdraw()
            self.options['title'] = 'Escoja nombre de archivo para guardar el registro'
            self.options['initialfile'] = self.fname
            filename = askopenfilename(**self.options)
            if filename == '' or not len(filename):
                raise Exception
        except:
            return False
        return filename


    def save(self):
        if self.is_new:
            fname = self.saveas_popup()
            if fname is False:
                return ''
            self.fname = fname
            self.is_new = False

        json_out = json.dumps(self.data)
        with open(self.fname, 'w') as f:
            f.write(json_out)
        return self.fname

    def load(self):
        fname = self.load_popup()
        if fname is False:
            return ''
        with open(fname, "r") as f:
            st = f.read()
            data = json.loads(st)
        self.data = data

        self.is_new = False
        self.fname = fname
        return fname
