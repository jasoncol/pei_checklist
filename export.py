# -*- coding: latin-1 -*-
"""Modulo para exportacion del registroo a otros formatos"""

from reportlab.lib.pagesizes import A4, cm
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib import colors
from kivy.factory import Factory
import time
from collections import OrderedDict

class PDFExporter(object):

    """Clase para exportar a pdf"""

    def export(self, registro, questions, fname, categories2report):
        """Exporta a pdf el registro
        :param fname: ruta y/o nombre del fichero pdf. Si no se especifica se usa reporte_[timestamp].pdf.

        :returns: ruta del archivo generado.
        """
        ktime = time.time()
        if fname is None:
            fname = "reporte_%s.pdf"%(time.time())
        self.registro = registro
        width, height = A4
        doc = SimpleDocTemplate(fname,
                                pagesize=A4, rightMargin=18, leftMargin=18,
                                topMargin=18, bottomMargin=18)
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name='answer', textColor=colors.blue))
        self.styles = styles
        Story = []
        # Agregar imagen de header
        #Story.append(Image("./img/header_pdf.png", 20 * cm)) # TODO imagen header
        # Agregar datos generales de registroo
        Story.append(
            Paragraph("<b>Herramienta para el diagnóstico y seguimiento del PEI</b>",
                      styles['Normal']))
        Story.append(Spacer(1, 12))
        Story.append(Spacer(1, 12))
        Story.append(
            Paragraph("<b>Datos generales del registro</b>",
                      styles['Normal']))
        Story.append(self.general_data())
        Story.append(Spacer(1, 12))
        Story.append(Spacer(1, 12))

        # Respuestas a preguntas
        Story.append(Paragraph("<b>Reporte</b>", styles['Normal']))


        #Reorganizar data
        cats = OrderedDict()

        for question in questions:
            if question.categoria in cats:
                cats[question.categoria].append(question)
            else:
                cats[question.categoria] = [question]


        for cat, questions in cats.iteritems():
            if cat not in categories2report:
                continue
            Story.append(Paragraph("<b>%s</b>"%(cat), styles['Normal']))
            data = []
            es =  [] # extra styles
            for i, question in enumerate(questions):

                row = []
                row.append(Paragraph("%s" % (question.titulo),  styles['answer']))
                row.append(Paragraph("%s" % (question.descripcion),  styles['Normal']))
                try:
                    respuesta = question.get_data()['SQID-1'][0]
                    respuesta = 'Si' if respuesta == 1 else 'No'
                    color = colors.green if respuesta =='Si' else colors.red
                except:
                    respuesta = '-'
                    color = colors.white

                es.append(['BACKGROUND',(2,i),(2,i),color])
                row.append(Paragraph("%s" % (respuesta),  styles['Normal']))
                data.append(row)

            Story.append(Spacer(1, 12))
            Story.append(self.create_table(data, 0, col_widths=[5 * cm, 9 * cm, 2 *cm], extra_styles=es))
            Story.append(Spacer(1, 12))

        Story.append(Paragraph("<b>Fin del Reporte</b>", styles['Normal']))
        '''    
        for question in questions:
            if question.categoria != last_cat:
                last_cat = question.categoria
                Story.append(
                Paragraph("<b>%s</b>"%(last_cat),
                      styles['Normal']))

            qdata = self.registro.get_question_data(question.qid)

                
            Story.append(Paragraph("%s %s" % (question.titulo, question.descripcion),
                          styles['Normal']))
            Story.append(Spacer(1, 12))
           
            Story.append(Spacer(1, 12))
            Story.append(Spacer(1, 12))
        '''
        doc.build(Story)
        print "FINALIZADO ET:", time.time() - ktime

        
        return fname


    def general_data(self):
        """Crea el cuadro de datos generales del reporte de registroo"""
        import time
        today = time.strftime("%d/%m/%Y %H:%M:%S")

        data = [
            [Paragraph("<b>Fecha de generación</b>", self.styles['BodyText']),
             Paragraph(today, self.styles['answer'])],
            [Paragraph("<b>Archivo del registro</b>",
                       self.styles['BodyText']),
             Paragraph(self.registro.fname, self.styles['answer'])],

        ]
        return self.create_table(data, firstrow_index=0)

    def create_table(self, data, firstrow_index=1, col_widths=None, extra_styles=None):
        """Crea una tabla y usando la informacion en data
        :param firstrow_index: indice de la primera fila donde estan las
        respuestas en la tabla. A veces es la primera fila (0)
        o la segunda (1)"""

        num_cols = len(data[0])
        colWidths = None
        if col_widths is not None:
            colWidths = col_widths
        elif num_cols > 2:
            col1_width = [4 * cm] if len(data[1][0].text) > 10 else [1 * cm]
            colWidths = col1_width + [3 * cm] * (num_cols - 1)

        elif num_cols >= 7:
            colWidths = col1_width + [2 * cm] * (num_cols - 1)

        t = Table(data, hAlign='LEFT', colWidths=colWidths)
        ts = TableStyle([('ALIGN', (0, 1), (-1, -1), 'LEFT'),
                               ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
                               ('INNERGRID', (0, 0),
                                (-1, -1), 0.25, colors.black),
                               ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
                               ('TEXTCOLOR', (1, firstrow_index),
                                (-1, -1), colors.blue)
                               ])
        if extra_styles is not None:
            for es in extra_styles:
                ts.add(*es)
        t.setStyle(ts)
        return t

    def format_text(self, text):
        text_formatted = text.replace('\n', '<br/>')
        return text_formatted
