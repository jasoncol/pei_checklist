from kivy.lang import Builder
from collections import OrderedDict

class KvPropertiesMixin(object):
    """Mixin para que una clase de un widget pueda obtener los valores de las propiedades
    definidos en las reglas kv"""

    @classmethod
    def get_kv_props(cls):
        clsname = cls.__name__ + '@'
        rule = [x[1] for x in Builder.rules if clsname in x[1].name][0]
        properties = OrderedDict(rule.properties)
        return properties, rule
